import React, {useState} from "react";
import {Button, Card, Checkbox} from "antd";
import classes from "./invoice.module.scss";
import {useTranslation} from "react-i18next";
import {Title} from "../../components";


const Invoice = () => {
    const {t} = useTranslation();

    return (
        <div className={classes.invoice}>
            <ul>
                <li>
                    <Title className={classes.titles} title={t("Итого")}/>
                    {/*<p className={classes.titles}>{t("Итого")}</p>*/}
                    <p className={classes.titles}>3.000.000 <span>{t("сўм")}</span></p>
                </li>
                <li>
                    <p>{t("Товары")}: 5 {t("шт")}</p>
                    <p>3.500.000 <span>{t("сўм")}</span></p>
                </li>
                <li>
                    <p>{t("Скидка")}</p>
                    <p>3.500.000 <span>{t("сўм")}</span></p>
                </li>
                <li style={{borderBottom: "1px solid #AAAAAA", marginBottom: 12}}>
                    <p>{t("Стоимость доставки")}</p>
                    <p>50.000 <span>{t("сўм")}</span></p>
                </li>
                <li className={classes.all_info} style={{alignItems: "start"}}>
                    <p>{t("Адр. доставки")}</p>
                    <p style={{maxWidth: 120}}>Г. Ташкент, улица Каратош 27А</p>
                </li>
                <li className={classes.all_info}>
                    <p>{t("Дата доставки")}</p>
                    <p>16-18 марта</p>
                </li>
                <li className={classes.all_info} style={{margin: "12px 0"}}>
                    <p>{t("Оплата")}</p>
                    <p>Картой</p>
                </li>
                <li className="align-start">
                    <Button className={classes.order_button} size={"large"}>
                        {t("Заказать")}
                    </Button>
                    <Checkbox checked={true} className={classes.terms_of_use}>
                        <span>{t("Согласен с условиями")} </span>
                        <a href="/assets/documents/Правила%20пользования.docx" download
                           style={{textDecoration: "underline"}}>
                            {t("Правил пользования")}
                        </a>
                    </Checkbox>
                </li>
            </ul>

        </div>
    )
}
export default Invoice;