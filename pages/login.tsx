import React from "react";
import {Card} from "antd";
import {Input} from "../components";
import {LoginContainer} from "../containers";

const Login = () => {
    return (
        <div style={{display: "flex", justifyContent: "center", alignItems: "center", marginTop: 100}}>
            <LoginContainer/>
        </div>
    )
}

export default Login;