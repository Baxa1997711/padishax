import {combineReducers, configureStore} from "@reduxjs/toolkit";
import {createWrapper} from "next-redux-wrapper";
import cardSlice from "./cardSlice";

const combinedReducer = combineReducers({
    cardSlice
})

export const makeStore: any = () => {
    configureStore({
        reducer: combinedReducer,
    })
}

export const wrapper = createWrapper(makeStore);


