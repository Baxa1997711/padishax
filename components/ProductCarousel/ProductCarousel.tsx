import React, {FC, useState} from "react";
import {Col, Row, Typography, Image as AntImage, Button} from "antd";
import Image from "next/image";
import classes from "./product-carousel.module.scss";
import {useTranslation} from "react-i18next";
import cn from "classnames";
import {HeartOutlined} from "@ant-design/icons";

interface ProductCarouselProps {
    photosList: string[];
}

const ProductCarousel: FC<ProductCarouselProps> = ({photosList}) => {
    const {Text} = Typography;
    const [activeCarousel, setActiveCarousel] = useState(photosList?.[0]);
    const styles = {backgroundColor: "#d6d5d1"};


    return (
        <div>
            <Row gutter={[10, 10]}>
                <Col span={24}>
                    <div className={cn(classes.active_gallery)}>
                        <Text className={classes.img_name}>Артикул: 93201</Text>
                        {activeCarousel && <Image src={activeCarousel} width={400} height={400}/>}
                        <Button className={classes.like} size={"large"} type={"text"} icon={<HeartOutlined/>}/>
                    </div>
                </Col>
                <Col span={24}>
                    <div className={classes.gallery_list}>
                        {photosList.map((photo) => (
                            <div style={activeCarousel === photo ? styles : {}}
                                 onClick={() => setActiveCarousel(photo)}
                                 className={classes.image} key={photo}>
                                <Image src={photo} width={65} height={65}/>
                            </div>
                        ))}
                    </div>
                </Col>
            </Row>
        </div>
    )

}
export default ProductCarousel;