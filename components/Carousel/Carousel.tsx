import React from "react";
import Slider from "react-slick";


const Carousel = () => {
    const settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false
    };
    return (
        <>
            <Slider {...settings}>
                <div>
                    <img width={"100%"} src="/assets/icons/slider_banner.png" alt="banner"/>
                </div>
                <div>
                    <img width={"100%"} src="/assets/icons/slider_banner.png" alt="banner"/>
                </div>
                <div>
                    <img width={"100%"} src="/assets/icons/slider_banner.png" alt="banner"/>
                </div>
            </Slider>
        </>
    )
}

export default Carousel;