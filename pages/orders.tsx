import React, {useEffect, useState} from "react";
import {Col, Row} from "antd";
import {Basket, DeliveryAddress, Invoice, PaymentMethod} from "../containers";
import {useWindowSize} from "../utils/use-window-size";
import {DEFAULT_DESKTOP_WINDOW_SIZE} from "../utils/constants";


const Orders = () => {
    const [order, setOrder] = useState([]);
    const {width} = useWindowSize();


    useEffect(() => {
        const ordersString = localStorage.getItem("order");
        const orders = JSON.parse(ordersString ? ordersString : "[]") || [];
        setOrder(orders);
    }, [])

    return (
        <div className={width > DEFAULT_DESKTOP_WINDOW_SIZE ? "container" : "mini_container"}>
            <Row gutter={[30, 30]}  style={{paddingBottom: 60, paddingTop: 30}}>
                <Col lg={{span: 16, order: 1}}>
                    <Basket order={order}/>
                </Col>
                <Col lg={{span: 8, order: 3}} xs={24}>
                    <DeliveryAddress/>
                </Col>
                <Col lg={{span: 8, order: 4}} xs={24}>
                    <PaymentMethod/>
                </Col>
                <Col lg={{span: 8, order: 2}} xs={24}>
                    <Invoice/>
                </Col>
            </Row>
        </div>
    )
}

export default Orders;