import {useQuery} from "react-query";
import {request} from "../../services/Api";

export const useProductList = () => {
    const query = useQuery("use-get-all-product-list", async () => {
        const {data} = await request.get(`/product/products-list`);
        return data;
    });
    return query;
};
