import React, {FC, InputHTMLAttributes} from "react";
import {Input as AntInput, InputProps, Select} from "antd";
import classes from "./input.module.scss";
import NumberFormat from "react-number-format";

type InputProp = InputProps & {
    field: string;
    format?: string;
    onValueChange?: any
}


const Input: FC<InputProp> = ({field, format, onValueChange, ...props}) => {

    return (
        <>
            {field === "format" &&
                <NumberFormat style={{width: "100%", height: 40, fontSize: 16, padding: "0 10px"}}
                              className={classes.input} format={format} onValueChange={onValueChange} {...props}/>}
            {field === "input" && <AntInput className={classes.input} size={"large"}  {...props}/>}
            {field === "select" && <div className={classes.select}><Select className={classes.input} size={"large"} {...props}/></div>}
        </>
    )
}

export default Input;