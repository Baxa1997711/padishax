import 'antd/dist/antd.css';
import '../styles/globals.css'
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import "../styles/typesense.scss";
import {Footer, Layout} from "../components";
import styles from '../styles/Home.module.css';
import {appWithTranslation} from "next-i18next";
import "services/i18n/index";
import {Hydrate, QueryClient, QueryClientProvider} from "react-query";
import {useRef} from "react";
import {wrapper} from "../store/store";
import Home from "./index";


function MyApp({Component, pageProps}) {
    const queryClientRef = useRef<any>();
    if (!queryClientRef.current) {
        queryClientRef.current = new QueryClient();
    }

    return (
        <>
            <main className={styles.main}>
                <QueryClientProvider client={queryClientRef.current}>
                    <Hydrate state={pageProps.dehydratedState}>
                        <Layout>
                            <Component {...pageProps} />
                        </Layout>
                    </Hydrate>
                </QueryClientProvider>
            </main>
            <footer className={styles.footer}>
                <Footer/>
            </footer>
        </>
    )

}

export default MyApp;
