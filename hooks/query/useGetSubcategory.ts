import {useQuery} from "react-query";
import {request} from "../../services/Api";

export const useGetSubcategory = () => {
    const query = useQuery("use-get-all-subcategory", async () => {
        const {data} = await request.get(`/subcategory/subcategories-list`);
        return data;
    });
    return query;
};
