import {Checkbox} from "antd";
import React from "react";
import {colors} from "../../utils/constants";
import {useTranslation} from "react-i18next";
import {RefinementList} from 'react-instantsearch-dom'
import {CustomRefinementList} from "../../components/CheckBoxGroup/CheckBoxGroup";


const styles: any = {
    textTransform: 'uppercase',
    fontSize: 16,
    fontFamily: "Roboto",
    marginBottom: 10
}


const SearchFilters = () => {
    const {t} = useTranslation();

    return (
        <div>
            <div style={{marginBottom: 50}}>
                <h3 style={styles}>{t("Категории")}</h3>
                <CustomRefinementList
                    className="mt-3"
                    attribute="category"
                    limit={10}
                    showMore={true}
                    showMoreLimit={50}
                    // transformItems={items =>
                    //     items.sort((a, b) => a.label > b.label ? 1 : -1)
                    // }
                />
                {/*<CheckBoxGroup filterLength={4} checkBoxData={checkBoxData}/>*/}
            </div>
            <div style={{marginBottom: 50}}>
                <h3 style={styles}>{t("Бренды")}</h3>
                <CustomRefinementList
                    className="mt-3"
                    attribute="brand"
                    limit={10}
                    showMore={true}
                    showMoreLimit={50}
                />
                {/*<CheckBoxGroup filterLength={11} checkBoxData={checkBoxData}/>*/}
            </div>
            <div style={{marginBottom: 50}}>
                <h3 style={styles}>{t("Цвет")}</h3>
                <CustomRefinementList
                    className="mt-3"
                    attribute="colors"
                    limit={10}
                    showMore={true}
                    showMoreLimit={50}
                />
                {/*<CheckBoxGroup filterLength={8} checkBoxData={colors}/>*/}
            </div>
            <div style={{marginBottom: 50}}>
                <h3 style={styles}>{t("Размер")}</h3>
                {/*<RefinementList*/}
                {/*    className="mt-3"*/}
                {/*    attribute="sizes"*/}
                {/*    limit={10}*/}
                {/*    // showMore={true}*/}
                {/*    showMoreLimit={50}*/}
                {/*    // transformItems={items =>*/}
                {/*    //     items.sort((a, b) => a.label > b.label ? 1 : -1)*/}
                {/*    // }*/}
                {/*/>*/}
                <CustomRefinementList
                    className="mt-3"
                    attribute="sizes"
                    limit={10}
                    showMore={true}
                    showMoreLimit={50}
                />
            </div>
            <div style={{marginBottom: 50}}>
                <h3 style={styles}>{t("Цены")}</h3>
                <CustomRefinementList
                    className="mt-3"
                    attribute="price"
                    limit={10}
                    showMore={true}
                    showMoreLimit={50}
                />
                {/*<CheckBoxGroup filterLength={4} checkBoxData={prices}/>*/}
            </div>
        </div>
    )
}

export default SearchFilters;