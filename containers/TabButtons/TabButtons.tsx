import React, {useEffect, useState} from "react";
import classes from "./tab-buttons.module.scss";
import {Button} from "antd";
import {HeartOutlined} from "@ant-design/icons";
import {useTranslation} from "react-i18next";
import cn from "classnames"
import {useRouter} from "next/router";
import {Loader} from "../../components";


const TabButtons = () => {
    const {t} = useTranslation();
    const router = useRouter();
    const [activeTabs, setActiveTabs] = useState(router?.query?.activeTabs);
    useEffect(() => {
        activeTabs !== undefined && router.push(`?activeTabs=${activeTabs}`)
    }, [activeTabs, router?.query?.activeTabs])




    return (
        <div className={classes.tab_buttons}>
            <Button style={activeTabs === "0" || router?.query?.activeTabs === "0" ? {border: "1px solid #374151", borderRadius: 5} : {}}
                    onClick={() => setActiveTabs("0")} className={cn("d-flex justify-start", classes.button)}
                    type={'text'}
                    icon={<img style={{marginRight: 10}} src="/assets/icons/user_icon.svg" alt="useIcon"/>}>
                {t("Личные данные")}
            </Button>
            <Button style={activeTabs === "1" || router?.query?.activeTabs === "1" ? {border: "1px solid #374151", borderRadius: 5} : {}}
                    onClick={() => setActiveTabs("1")} className={cn("d-flex justify-start", classes.button)}
                    type={'text'}
                    icon={<img style={{marginRight: 10}} src="/assets/icons/order.svg" alt="order-icon"/>}>
                {t("Заказы")}
            </Button>
            <Button style={activeTabs === "2" || router?.query?.activeTabs === "2" ? {border: "1px solid #374151", borderRadius: 5} : {}}
                    onClick={() => setActiveTabs("2")} className={cn("d-flex justify-start", classes.button)}
                    type={'text'}
                    icon={<HeartOutlined style={{fontSize: 20}}/>}>
                {t("Избранные")}
            </Button>
        </div>
    )
}

export default TabButtons;