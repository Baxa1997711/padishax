import React, {Dispatch, SetStateAction} from "react";
import {Drawer} from "antd";
import {CloseOutlined, YuqueOutlined} from "@ant-design/icons";
import CategoryMenu from "./_components/CategoryMenu";
import {useWindowSize} from "../../../../utils/use-window-size";
import MobileCategoryMenu from "./_components/MobileCategoryMenu";

type MenuDrawerProps = {
    setVisible: Dispatch<SetStateAction<boolean>>;
    visible: boolean;
}

const category = [
    {
        key: "1",
        icon: <YuqueOutlined/>,
        label: "Женская обувь",
    },
    {
        key: "2",
        icon: <YuqueOutlined/>,
        label: "Мужская обувь",
    },
    {
        key: "3",
        icon: <YuqueOutlined/>,
        label: "Детская обувь",
    },
    {
        key: "4",
        icon: <YuqueOutlined/>,
        label: "Красота",
    },
];

const MenuDrawer = ({setVisible, visible}: MenuDrawerProps) => {
    const {width} = useWindowSize();


    return (
        <Drawer
            title=""
            placement={"left"}
            width={width > 576 ? 550 : 320}
            closable={false}
            onClose={() => setVisible(false)}
            visible={visible}
            key={"left"}
            extra={<div onClick={() => setVisible(false)} style={{cursor: "pointer"}}>
                <CloseOutlined/>
            </div>}
        >
            {width > 576 ? <CategoryMenu/> :
                <MobileCategoryMenu setVisible={setVisible} visible={visible} items={category}/>}
        </Drawer>
    )
}

export default MenuDrawer;