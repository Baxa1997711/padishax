import NumberFormat, {NumberFormatProps} from 'react-number-format';
import {FC} from "react";

interface FormatNumberProps {
    num: number;
    thousandSeparator?: string;
    props?: NumberFormatProps;
}

const FormatNumber: FC<FormatNumberProps> = ({num, thousandSeparator = " ", ...props}) => {

    return (
        <NumberFormat value={num} displayType={'text'} thousandSeparator={thousandSeparator} {...props}/>
    )
}

export default FormatNumber