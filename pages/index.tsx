import Head from 'next/head'
import styles from '../styles/Home.module.css'
import {Layout, Carousel, Container, Footer} from "../components";
import CategoryCard from "../components/CategoryCard";
import {Col, Row} from "antd";
import {TopSells} from "../containers";
import {Toaster} from "react-hot-toast";
import {useWindowSize} from "../utils/use-window-size";
import {useTranslation} from "react-i18next";
import {InstantSearch} from "react-instantsearch-dom"
import TypesenseInstantSearchAdapter from "typesense-instantsearch-adapter";
import {assembleTypesenseServerConfig} from "../utils/assembleTypesenseServerConfig";
import {findResultsState} from "react-instantsearch-dom/server";

const TYPESENSE_SERVER_CONFIG = assembleTypesenseServerConfig();


const typesenseInstantsearchAdapter = new TypesenseInstantSearchAdapter({
    server: TYPESENSE_SERVER_CONFIG,
    additionalSearchParameters: {
        // The following parameters are directly passed to Typesense's search API endpoint.
        //  So you can pass any parameters supported by the search endpoint below.
        //  queryBy is required.
        queryBy: 'name,category',
        // queryByWeights: '4,2,1',
        numTypos: 1,
        typoTokensThreshold: 1,
        // groupBy: "categories",
        // groupLimit: 1
        // pinnedHits: "23:2"
    },
});


export default function Home(
    {
        searchClient = typesenseInstantsearchAdapter.searchClient,
        searchState,
        resultsState,
        onSearchParameters,
        widgetsCollector,
        ...props
    }) {
    const {t} = useTranslation();
    const categoryData = [
        {
            title: t("Женская обувь"),
            img: "/assets/categoryCard/category_photo_1.jpg",
        },
        {
            title: "Мужская обувь",
            img: "/assets/categoryCard/category_photo_2.jpg",
        },
        {
            title: "Обувь для девочек",
            img: "/assets/categoryCard/category_photo_4.png",
        },
        {
            title: "Обувь для мальчиков",
            img: "/assets/categoryCard/category_photo_3.png",
        },
    ];
    const {width} = useWindowSize();

    const products = [
        {
            key: 1,
            img: "/assets/shoes.png",
            description: "Striped Flutter Sleeve Striped Flutter Sleeve...",
            price: 1200000
        },
        {
            key: 2,
            img: "/assets/shoes.png",
            description: "Striped Flutter Sleeve Striped Flutter Sleeve...",
            price: 1200000
        },
        {
            key: 3,
            img: "/assets/shoes.png",
            description: "Striped Flutter Sleeve Striped Flutter Sleeve...",
            price: 1200000
        },
        {
            key: 4,
            img: "/assets/shoes.png",
            description: "Striped Flutter Sleeve Striped Flutter Sleeve...",
            price: 1200000
        },
        {
            key: 5,
            img: "/assets/shoes.png",
            description: "Striped Flutter Sleeve Striped Flutter Sleeve...",
            price: 1200000
        },
        {
            key: 6,
            img: "/assets/shoes.png",
            description: "Striped Flutter Sleeve Striped Flutter Sleeve...",
            price: 1200000
        },
        {
            key: 7,
            img: "/assets/shoes.png",
            description: "Striped Flutter Sleeve Striped Flutter Sleeve...",
            price: 1200000
        },
        {
            key: 8,
            img: "/assets/shoes.png",
            description: "Striped Flutter Sleeve Striped Flutter Sleeve...",
            price: 1200000
        },
        {
            key: 9,
            img: "/assets/shoes.png",
            description: "Striped Flutter Sleeve Striped Flutter Sleeve...",
            price: 1200000
        },
        {
            key: 10,
            img: "/assets/shoes.png",
            description: "Striped Flutter Sleeve Striped Flutter Sleeve...",
            price: 1200000
        },
        {
            key: 11,
            img: "/assets/shoes.png",
            description: "Striped Flutter Sleeve Striped Flutter Sleeve...",
            price: 1200000
        },
        {
            key: 12,
            img: "/assets/shoes.png",
            description: "Striped Flutter Sleeve Striped Flutter Sleeve...",
            price: 1200000
        },
        {
            key: 13,
            img: "/assets/shoes.png",
            description: "Striped Flutter Sleeve Striped Flutter Sleeve...",
            price: 1200000
        },
        {
            key: 14,
            img: "/assets/shoes.png",
            description: "Striped Flutter Sleeve Striped Flutter Sleeve...",
            price: 1200000
        },
        {
            key: 15,
            img: "/assets/shoes.png",
            description: "Striped Flutter Sleeve Striped Flutter Sleeve...",
            price: 1200000
        },
    ];
    const brandProducts = [
        {
            key: 1,
            img: "/assets/shoes.png",
            description: "Striped Flutter Sleeve Striped Flutter Sleeve...",
            price: 1200000
        },
        {
            key: 2,
            img: "/assets/shoes.png",
            description: "Striped Flutter Sleeve Striped Flutter Sleeve...",
            price: 1200000
        },
        {
            key: 3,
            img: "/assets/shoes.png",
            description: "Striped Flutter Sleeve Striped Flutter Sleeve...",
            price: 1200000
        },
        {
            key: 4,
            img: "/assets/shoes.png",
            description: "Striped Flutter Sleeve Striped Flutter Sleeve...",
            price: 1200000
        },
        {
            key: 5,
            img: "/assets/shoes.png",
            description: "Striped Flutter Sleeve Striped Flutter Sleeve...",
            price: 1200000
        },
        {
            key: 6,
            img: "/assets/shoes.png",
            description: "Striped Flutter Sleeve Striped Flutter Sleeve...",
            price: 1200000
        },
        {
            key: 7,
            img: "/assets/shoes.png",
            description: "Striped Flutter Sleeve Striped Flutter Sleeve...",
            price: 1200000
        },
        {
            key: 8,
            img: "/assets/shoes.png",
            description: "Striped Flutter Sleeve Striped Flutter Sleeve...",
            price: 1200000
        },
        {
            key: 9,
            img: "/assets/shoes.png",
            description: "Striped Flutter Sleeve Striped Flutter Sleeve...",
            price: 1200000
        },
        {
            key: 10,
            img: "/assets/shoes.png",
            description: "Striped Flutter Sleeve Striped Flutter Sleeve...",
            price: 1200000
        },
        {
            key: 11,
            img: "/assets/shoes.png",
            description: "Striped Flutter Sleeve Striped Flutter Sleeve...",
            price: 1200000
        },
        {
            key: 12,
            img: "/assets/shoes.png",
            description: "Striped Flutter Sleeve Striped Flutter Sleeve...",
            price: 1200000
        },
        {
            key: 13,
            img: "/assets/shoes.png",
            description: "Striped Flutter Sleeve Striped Flutter Sleeve...",
            price: 1200000
        },
        {
            key: 14,
            img: "/assets/shoes.png",
            description: "Striped Flutter Sleeve Striped Flutter Sleeve...",
            price: 1200000
        },
        {
            key: 15,
            img: "/assets/shoes.png",
            description: "Striped Flutter Sleeve Striped Flutter Sleeve...",
            price: 1200000
        },
        {
            key: 16,
            img: "/assets/shoes.png",
            description: "Striped Flutter Sleeve Striped Flutter Sleeve...",
            price: 1200000
        },
        {
            key: 17,
            img: "/assets/shoes.png",
            description: "Striped Flutter Sleeve Striped Flutter Sleeve...",
            price: 1200000
        },
        {
            key: 18,
            img: "/assets/shoes.png",
            description: "Striped Flutter Sleeve Striped Flutter Sleeve...",
            price: 1200000
        },
        {
            key: 19,
            img: "/assets/shoes.png",
            description: "Striped Flutter Sleeve Striped Flutter Sleeve...",
            price: 1200000
        },
        {
            key: 20,
            img: "/assets/shoes.png",
            description: "Striped Flutter Sleeve Striped Flutter Sleeve...",
            price: 1200000
        },
    ];
    const brands = [
        {
            img: "/assets/brands/brands_1.png"
        },
        {
            img: "/assets/brands/brands_2.png"
        },
        {
            img: "/assets/brands/brands_3.png"
        },
        {
            img: "/assets/brands/brands_4.png"
        },
        {
            img: "/assets/brands/brands_5.png"
        },
        {
            img: "/assets/brands/brands_6.png"
        }
    ];


    return (
        <InstantSearch indexName="products"
                       {...props}
                       searchClient={searchClient}
                       searchState={searchState}
                       resultsState={resultsState}
                       onSearchParameters={onSearchParameters}
                       widgetsCollector={widgetsCollector}
        >
            <Head>
                <title>padishah shoes</title>
                <meta name="description" content="Padishah - Shoes"/>
                <link rel="icon" href="/favicon.ico"/>
            </Head>
            <Toaster
                position="bottom-center"
                reverseOrder={false}
            />

            <div className={"container"} id="category">
                {/*<Carousel/>*/}
                <Row gutter={[30, 30]}>
                    {categoryData?.map(({img, title}, key) => {
                        return (
                            <Col md={6} xs={12} key={key} >
                                <CategoryCard
                                    img={img}
                                    title={title}
                                />
                            </Col>
                        )
                    })}
                </Row>
                <TopSells products={products} title={t("Рекомендуем")} />
                <div className="" id='top_sells_brands'>
                <TopSells products={brandProducts} title={"Бренды"} brands={brands} pagination={true}/>
                </div>
            </div>
        </InstantSearch>
    )
}

export async function getServerSideProps({res}) {
    res.setHeader("Cache-Control", `s-maxage=${1 * 60 * 60}, stale-while-revalidate=${24 * 60 * 60}`);

    const resultsState = await findResultsState(Home, {
        indexName: "products",
        searchClient: typesenseInstantsearchAdapter.searchClient
    });

    return {
        props: {
            resultsState: JSON.parse(JSON.stringify(resultsState)),
        }
    }
}
