import React from "react";
import classes from "./loader.module.scss";


const Loader = () => {

    return (
        <div className={classes.loader}>
            Loading...
        </div>
    )
}

export default Loader;