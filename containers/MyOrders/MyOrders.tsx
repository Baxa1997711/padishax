import React from "react";
import {EachMyOrder, Title} from "../../components";
import {useTranslation} from "react-i18next";


const MyOrders = () => {
    const {t} = useTranslation()
    const eachMyOrderData = Array(3).fill(
        {
            orderNumber: 21564655,
            orderDate: "21-09-2022 в 16:00",
            orderPrice: 3500250,
            deliveryAddress: "г. Ташкент р. Мирзо улугбек ул. Коратош 45\\43",
            deliveryDate: "16-18 марта 2022 года",
            paymentStatus: "payed",
            orderStatus: ""
        }
    )


    return (
        <div style={{marginBottom: 80}}>
            <Title title={t("Мои заказы")}/>
            {eachMyOrderData?.map((item, index) => (
                <EachMyOrder {...{index, ...item}} key={index}/>
            ))}
        </div>
    )
}

export default MyOrders;