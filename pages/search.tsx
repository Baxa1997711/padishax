import React from "react";
import {Breadcrumb, Container, Loader, Title} from "../components";
import {useTranslation} from "react-i18next";
import {Button, Col, Dropdown, Menu, Row} from "antd";
import {ArrowLeftOutlined, AppstoreOutlined} from "@ant-design/icons"
import {ProductList, SearchFilters, SearchFilterSection} from "../containers";
import {DEFAULT_DESKTOP_WINDOW_SIZE, DEFAULT_MOBILE_WINDOW_SIZE, server} from "../utils/constants";
import {useWindowSize} from "../utils/use-window-size";
import {useRouter} from "next/router";
import {InstantSearch} from "react-instantsearch-dom"
import TypesenseInstantSearchAdapter from "typesense-instantsearch-adapter";
import {assembleTypesenseServerConfig} from "../utils/assembleTypesenseServerConfig";
import {findResultsState} from "react-instantsearch-dom/server";

const TYPESENSE_SERVER_CONFIG = assembleTypesenseServerConfig();


const typesenseInstantsearchAdapter = new TypesenseInstantSearchAdapter({
    server: TYPESENSE_SERVER_CONFIG,
    additionalSearchParameters: {
        // The following parameters are directly passed to Typesense's search API endpoint.
        //  So you can pass any parameters supported by the search endpoint below.
        //  queryBy is required.
        queryBy: 'name,category',
        // queryByWeights: '4,2,1',
        numTypos: 1,
        typoTokensThreshold: 1,
        // groupBy: "categories",
        // groupLimit: 1
        // pinnedHits: "23:2"
    },
});

const Search = ({
                    searchClient = typesenseInstantsearchAdapter.searchClient,
                    searchState,
                    resultsState,
                    onSearchParameters,
                    widgetsCollector,
                    ...props
                }) => {
    const {t} = useTranslation()
    // const productList = useProductList();
    const {width} = useWindowSize();
    const router = useRouter();

    const productList = {
        data: Array(10).fill({
            productId: 2176,
            photo: ["/assets/shoes.png"],
            description: "fisdf iufisdfbsdhbf isbfusbd isdbfusbd shkdhbfusdbf sdfbusdfbsduhsdsudysd fsud",
            price: 12321
        }),
        isLoading: false
    }

    const breadcrumb = [
        {
            title: "Главная",
            path: "/"
        },
        {
            title: "Мужская обувь",
            path: "/search"
        },
        {
            title: "Кроссовки",
        }
    ];


    if (productList.isLoading) {
        return <div className="d-flex justify-center">
            <Loader/>
        </div>
    }

    return (
        <InstantSearch indexName="products"
                       {...props}
                       searchClient={searchClient}
                       searchState={searchState}
                       resultsState={resultsState}
                       onSearchParameters={onSearchParameters}
                       widgetsCollector={widgetsCollector}
        >
            <Container className={width > DEFAULT_DESKTOP_WINDOW_SIZE ? "container" : "mini_container"}>
                {width > DEFAULT_DESKTOP_WINDOW_SIZE && <div className="d-flex" style={{paddingTop: 20}}>
                    <Button icon={<ArrowLeftOutlined/>} style={{
                        border: "1px solid #374151",
                        borderRadius: 5,
                        marginRight: 15
                    }} onClick={() => router.back()}>{t("Назад")}</Button>
                    <Breadcrumb breadCrumb={breadcrumb}/>
                </div>}
                {width > DEFAULT_DESKTOP_WINDOW_SIZE ? <Title title={t("Мужская обувь.")}/> :
                    <Row gutter={20} style={{alignItems: "center", marginBottom: 20}}>
                        <Col span={2}>
                            <ArrowLeftOutlined style={{fontSize: 22}}/>
                        </Col>
                        <Col span={22} className="d-flex justify-center">
                            <Title className={"text-center"}
                                   title={t("Мужская обувь")}/>
                        </Col>
                    </Row>}
            </Container>
            {width < DEFAULT_MOBILE_WINDOW_SIZE && <SearchFilterSection/>}
            <Container className={width > DEFAULT_DESKTOP_WINDOW_SIZE ? "container" : "mini_container"}>
                {width > DEFAULT_DESKTOP_WINDOW_SIZE ? <Row className="align-start" gutter={[50, 30]}>
                    <Col span={5}>
                        <SearchFilters/>
                    </Col>
                    <Col span={19}>
                        <ProductList key={"product-list"} productList={productList?.data}/>
                    </Col>
                </Row> : <ProductList key={"product-list"} productList={productList?.data}/>}
            </Container>
        </InstantSearch>
    );
}

export async function getServerSideProps({res}) {
    res.setHeader("Cache-Control", `s-maxage=${1 * 60 * 60}, stale-while-revalidate=${24 * 60 * 60}`);

    const resultsState = await findResultsState(Search, {
        indexName: "products",
        searchClient: typesenseInstantsearchAdapter.searchClient
    });

    return {
        props: {
            resultsState: JSON.parse(JSON.stringify(resultsState)),
        }
    }
}


export default Search;